# Eventio 
![NPM Version][npm-image]

> A web app that allows registered users to sign up for and create events

This project is currently under development and requires more changes, please see the [Remaining Tasks & Wishlist]() section if you want to contribute!

![](example.png)

## Hosting 

https://strv-eventio-mc.surge.sh

## Installation

OS X & Linux:

```sh
git clone https://bitbucket.org/MCollJ/eventio.git
cd eventio
npm install
```

## Test Accounts
| Email| Password|
| ------------- |:-------------:|
brucebanner@strv.com | kill3r
steverogers@strv.com | am3riCa
thor@strv.com | missMyBroth3r
peterparker@strv.com | hat3Spid3rs
blackwidow@strv.com| l0veLateX
buckybarnes@strv.com|darkS0ldier
tonystark@strv.com|ir0nL0ver


## Remaining Tasks & Wishlist

### Pages (in order of importance)
_The pages below are yet to be implemented, click the links below to see each respective design spec on Zeplin._

* [Sign Up](https://app.zeplin.io/project/5915b42f450de421389e2ddb/screen/5915b64bdc93813adfd64db3)
* [Forgot Password](https://app.zeplin.io/project/5915b42f450de421389e2ddb/screen/5915b64b0bd757353a40a515)
* [Edit Event](https://app.zeplin.io/project/5915b42f450de421389e2ddb/screen/5915b70f39e03991dc35c3e3)
* [Generic Errors](https://app.zeplin.io/project/5915b42f450de421389e2ddb/screen/5915b6aa0bd757353a40ac84) 
* [Event Detail](https://app.zeplin.io/project/5915b42f450de421389e2ddb/screen/5915b6517d948ed36c8dbbf0)
* [Event Detail Web](https://app.zeplin.io/project/5915b42f450de421389e2ddb/screen/5915b70b09d68d736faf9f3b)
* [User Profile](https://app.zeplin.io/project/5915b42f450de421389e2ddb/screen/5915b65177e97603e1f941f9)
* [User Profile Web](https://app.zeplin.io/project/5915b42f450de421389e2ddb/screen/5915b65177e97603e1f941f9)

### Features (in order of importance)
* **Form Feedback** _The styling on form validation errors does not match the style guide provided :( this should be fixed ASAP_
* **Efficient Event Loading** _We should only render as many events as can fit on the page, this could be automatically as we scroll or a "load more" button_
* **Loading Spinners**  _It would be nice to have some loading spinners for components waiting for asynch requests_
* **Tests** _Component tests using [Jest](https://jestjs.io/) or similar_
* **Filtering & Sorting** _More filtering/sorting options like Owner, date range etc._
* **ButtonSwitcher** _ButtonSwitcher component should be made more generic_
* **CSS Cleanup** _CSS Could be better structured and ideally implemented with SASS_

### Bugs
* **Auth.InvalidToken** _There is still some occasional strange behaviour when logged in on multiple browsers and authenticating_


## Notes & Info

### Styleguide
This project has been built with Atomic Design in mind and loosely reflects that structure. Following this pattern when creating new components in this project is recommended.

_For more information on Atomic Design, [click here](http://bradfrost.com/blog/post/atomic-web-design/)._

### Misc

Any questions? contact me 
here : Michael Collins – michaeljosefcollins@gmail.com

Built as test project for [STRV Prague](https://strv.com). 


[npm-image]: https://img.shields.io/badge/npm-v6.4.1-orange.svg