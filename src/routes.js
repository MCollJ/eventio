import React from 'react';
import { Switch, Route, Redirect} from 'react-router-dom'

import Home from './components/pages/Home'
import Stylesheet from './components/pages/Stylesheet'
import LoginPage from './components/pages/LoginPage'
import CreatePage from './components/pages/CreatePage'
import auth from './auth'
import ErrorPage from './components/pages/ErrorPage';

const Routes = (props) => {
  return(
    <Switch>
      <Route exact component={LoginPage} path="/login"/>
      <PrivateRoute exact component={Home} path="/"/>
      <PrivateRoute component={CreatePage} path="/create"/>
      <PrivateRoute component={Stylesheet} path="/stylesheet"/>
      <Route component={CatchAllPage} />
    </Switch>
  )
}

//A protected route that redirects to login if user is not authenticated
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    auth.hasToken() === true
      ? <Component {...props} />
      : <Redirect to={{
          pathname: '/login',
          state: { from: props.location }
        }} />
  )} />
);

const CatchAllPage = () => (
  <ErrorPage heading="404 Error - page not found">
    Seems like Darth Vader just hit our website and dropped it down.
    Please press the refresh button and everything should be fine again.
  </ErrorPage>
)

export default Routes;