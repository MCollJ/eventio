import axios from 'axios';
import auth from './auth'
import Auth from './auth';

const createAxiosInstance = () => axios.create({
  baseURL: 'https://testproject-api-v2.strv.com',
  timeout: 5000,
  headers: getHeaders()})

const baseInstance = createAxiosInstance()

baseInstance.interceptors.response.use(null, (error) => {
  //User.InvalidToken indicated expired JWT token
  if(error.response && error.config && error.response.data && error.response.data.error === "Auth.InvalidToken"){
    Auth.clearTokens()
    window.location.reload()
  }

  //User.NotAuthenticated indicates expired auth token
  if(error.response && error.config && error.response.data && error.response.data.error === "User.NotAuthenticated"){
    const originalRequest = error.config
    refreshTokenAndReRequest(originalRequest)
  }

  return Promise.reject(error)
})

async function refreshTokenAndReRequest(originalRequest) {
  const response = await axios.post("https://testproject-api-v2.strv.com/auth/native", {refreshToken : auth.getRefreshToken()}, {headers: {"APIKey" : "xcTkAhQWlt2Cq+nl5al/v6ajPnFzu+jwB4fZ2agVfW0s"}})
  const newToken = response.headers['authorization']
  //Update stored token
  auth.setAuthToken(newToken)
  //Update auth token in original request
  originalRequest.headers.authorization = newToken
  //Redo original request
  createAxiosInstance().request(originalRequest)
}

function getHeaders(){
  const headers = {"APIKey" : "xcTkAhQWlt2Cq+nl5al/v6ajPnFzu+jwB4fZ2agVfW0s"}
  const token = auth.getAuthToken()
  if(token){headers.Authorization = token}
  return headers;
}

export default baseInstance;