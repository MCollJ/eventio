export function formatISODate(ISODate) {
  return new Date(ISODate).toLocaleString("en-US", {
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "2-digit",
    minute: "numeric"
  });
}
