import React from 'react';
import {withRouter} from 'react-router-dom'
import PropTypes from 'prop-types';

import DropDown from '../molecules/DropDown'
import {MenuItem} from '../molecules/PopupMenu'
import auth from '../../auth'

import './UserDropDown.css'

const UserDropDown = (props) => {
  const user = auth.getUser()
  return (
    <span className={props.className}>
      <div className="dropdown_placeholder">{getInitials(user)}</div>
        <DropDown showText={props.showText} text={user.firstName+ " " + user.lastName}>
          <MenuItem text="Profile" />
          <LogOutMenuItem />
        </DropDown>
    </span>
  );
};

const getInitials = (user) => {
  return user.firstName.charAt(0) + user.lastName.charAt(0)
}

const LogOutMenuItem = withRouter(({ history }) => (
  <MenuItem text="Log out" onClick={() => {
    auth.clearTokens()
    history.push('/login')}}/>)
)

export default UserDropDown;

UserDropDown.propTypes = {
  showText : PropTypes.bool
}