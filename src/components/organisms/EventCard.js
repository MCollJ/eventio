import React, { Component } from "react";
import PropTypes from 'prop-types';

import { Heading, SubHeading, Text, IconText } from "../atoms/Text";
import { Button } from "../atoms/Buttons";

import {formatISODate} from '../../utils';
import Auth from "../../auth";

import "./EventCard.css";

class EventCard extends Component {

  render() {
    const event = this.props.event;
    return !this.props.list ? (
      <Card event={event} leaveHandler={this.props.leaveHandler} joinHandler={this.props.joinHandler}/>
    ) : (
      <ListCard event={event} leaveHandler={this.props.leaveHandler} joinHandler={this.props.joinHandler} />
    );
  }

}

const ListCard = ({ event, leaveHandler, joinHandler, ...props }) => (
  <div className="event_list_card">
    <SubHeading className="title">{event.title}</SubHeading>
    <Text className="description">{event.description}</Text>
    <SubHeading className="owner">
      {event.owner.firstName} {event.owner.lastName}
    </SubHeading>
    <SubHeading className="date">{formatISODate(event.startsAt)}</SubHeading>
    <Text className="capacity">
      {event.attendees.length} of {event.capacity}
    </Text>
    <EventButton event={event} leaveHandler={leaveHandler} joinHandler={joinHandler} />
  </div>
);

const Card = ({ event,leaveHandler, joinHandler, ...props }) => (
  <div className="event_card">
    <SubHeading className="starts_at">{formatISODate(event.startsAt)}</SubHeading>
    <Heading className="heading">{event.title}</Heading>
    <SubHeading className="owner">
      {event.owner.firstName} {event.owner.lastName}
    </SubHeading>
    <Text className="description">{event.description}</Text>
    <span className="event_card_bottom">
      <IconText icon="person">
        {event.attendees.length} of {event.capacity}
      </IconText>
      <EventButton event={event} leaveHandler={leaveHandler} joinHandler={joinHandler} />
    </span>
  </div>
);

const EventButton = ({ event, leaveHandler, joinHandler, ...props }) => {
  const userID = Auth.getUser().id;
  //Current user is owner
  if (userID === event.owner.id) {
    return <Button grey text="EDIT" />;
  }
  //Current user is registered for event
  if (event.attendees.find(attendee => attendee.id === userID)) {
    return (
      <Button onClick={() => leaveHandler(event.id)} pink text="LEAVE" />
    );
  }
  //Current user is neither owner nor registered
  return <Button onClick={() => joinHandler(event.id)} text="JOIN" />;
}

export default EventCard;

EventCard.propTypes = {
  event : PropTypes.object,
  list : PropTypes.bool,
  leaveHandler : PropTypes.func,
  joinHandler : PropTypes.func
}