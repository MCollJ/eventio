import React, { Component } from 'react';
import MediaQuery from 'react-responsive'

import EventCard from '../organisms/EventCard'
import './EventCardsContainer.css'

import Api from '../../api'
import ButtonSwitcher, {SwitchButton} from '../molecules/ButtonSwitcher';

import GridIconSelected from '../../resources/icons/grid_selected.png'
import GridIcon from '../../resources/icons/grid.png'
import ListIconSelected from '../../resources/icons/list_selected.png'
import ListIcon from '../../resources/icons/list.png'
import { ImageButton } from '../atoms/Buttons';


import DropDown from '../molecules/DropDown';
import { MenuItem } from '../molecules/PopupMenu';
import { Text } from '../atoms/Text';

class EventCardsContainer extends Component {
  constructor(props){
    super(props);
    this.state={
      events : [],
      filter: "all",
      list: false
    }
  }

  componentDidMount() {
    Api.get("/events")
    .then(response => {
        this.setState({events: response.data})
      })
  }

  showEvents(events) {
    //Apply current filter to events, sort by start date then map to card
    return events
      .filter((event) => this.filterEvent(this.state.filter, event))
      .sort((a,b) => a.startsAt > b.startsAt ? 1 : -1)
      .map((event) => <EventCard list={this.state.list} key={event.id} event={event} leaveHandler={this.leaveHandler} joinHandler={this.joinHandler} />)
    }

  leaveHandler = async (eventId) => {
    const response = await Api.delete(`events/${eventId}/attendees/me`)
    //Replace the existing event with one returned from server
    this.updateEvents(response.data)
  }

  joinHandler = async (eventId) => {
    const response = await Api.post(`events/${eventId}/attendees/me`)
    //Replace the existing event with one returned from server
    this.updateEvents(response.data)
  }

  updateEvents(targetEvent){
    //Remove existing event if present
    const newEvents = this.state.events.filter((event) => event.id !== targetEvent.id)
    //Add new event
    newEvents.push(targetEvent)
    this.setState({events : newEvents})
  }

  filterEvent(filter, event){
    switch(filter){
      case 'future':
        return new Date(event.startsAt) > Date.now()
      case 'past':
        return new Date(event.startsAt) < Date.now()
      default:
        return true;
    }
  }

  setFilter = (newState) =>{
    this.setState({
      filter : newState
    })
  }

  setListView = (newState) => {
    this.setState({
      list : newState
    })
  }

  render() {
    return (
      <div className="events_wrapper">
        <div className="events_header">

          <MediaQuery minWidth={865}>
            <ButtonSwitcher>
              <SwitchButton className={`${this.state.filter === "all" ? 'selected' : ''}`} text="ALL EVENTS" onClick={() => this.setFilter("all")}/>
              <SwitchButton className={`${this.state.filter === "future" ? 'selected' : ''}`} text="FUTURE EVENTS" onClick={() => this.setFilter("future")}/>
              <SwitchButton className={`${this.state.filter === "past" ? 'selected' : ''}`} text="PAST EVENTS" onClick={() => this.setFilter("past")} />
            </ButtonSwitcher>
          </MediaQuery>

          <MediaQuery maxWidth={864}>
            <Text className="event_filter_text">SHOW: </Text>
            <DropDown className="event_filter_dropdown" showText={true} text={`${this.state.filter.toUpperCase()} EVENTS`}>
              <MenuItem text="ALL" onClick={() => this.setFilter("all")} />
              <MenuItem text="FUTURE"  onClick={() => this.setFilter("future")}/>
              <MenuItem text="PAST"  onClick={() => this.setFilter("past")}/>
            </DropDown>
          </MediaQuery>

          <ButtonSwitcher className="list_switcher">
            <ImageButton className="inline switcher_icon_button" image={`${this.state.list ? GridIcon : GridIconSelected}`} onClick={() => this.setListView(false)} alt="grid icon" />
            <ImageButton image={`${this.state.list ? ListIconSelected : ListIcon}`}  onClick={() => this.setListView(true)} className="switcher_icon_button" alt="list icon" />
          </ButtonSwitcher>

        </div>
        <div className="events_container">
          {this.showEvents(this.state.events)}
        </div>
      </div>
    );
  }
}

export default EventCardsContainer;