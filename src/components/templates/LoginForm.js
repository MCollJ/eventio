import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

import './LoginForm.css'
import {Heading, SubHeading, Text} from '../atoms/Text'
import {Input} from '../atoms/Inputs'
import { TextButton, Button } from '../atoms/Buttons';

import Auth from '../../auth'
import Api from '../../api'


class LoginForm extends Component {
  constructor(props){
    super(props)
    
    this.state = {
      user: {
        email: '',
        password: ''
      },
      authError: false,
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  onSubmit(event){
    event.preventDefault();
    Api.post("/auth/native",{
      email: this.state.user.email,
      password: this.state.user.password
    })
    .then((response) => {
        Auth.setAuthToken(response.headers["authorization"])
        Auth.setRefreshToken(response.headers["refresh-token"])
        Auth.setUser(response.data)
        this.props.setRedirect()
      })
    .catch((error) => {
      if(error.response && error.response.data && error.response.data.error === "User.InvalidPassword")
       this.setState({authError:true})
    
    })
  }


  onChange(event){
    const field = event.target.name;
    const user = this.state.user;
    user[field] = event.target.value;

    this.setState({user});
  }

  render() {
    return (
      <form className="login_form" onSubmit={this.onSubmit} action="#">
        <Heading className="login_form_heading">Sign in to Eventio.</Heading>
        {this.state.authError > 0 ?
        <SubHeading className="error_font_color">Oops! That email and pasword combination is not valid.</SubHeading>
        : <SubHeading>Enter your details below.</SubHeading>}
        <Input name="email" type="email" onChange={this.onChange} hint="Email"/>
        <Input name="password" type="password" onChange={this.onChange} hint="Password"/>
        <div className="options_container">
          <TextButton lightgrey type="submit" text="Forgot password?"/>
          <MediaQuery maxWidth={865}>
          <br/>
              <Text className="inline light_grey_font">Don't have an account? </Text>
              <TextButton className="bold" grey text="SIGN UP"/>
          </MediaQuery>
          <br/><br/>
          <Button big text="SIGN IN"/>
        </div>
    </form>
    );
  }
}

export default LoginForm;

LoginForm.propTypes = {
  setRedirect : PropTypes.func
}