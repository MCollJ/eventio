import React from "react";
import PropTypes from 'prop-types';

import {Heading, SubHeading} from '../atoms/Text'
import {Input} from '../atoms/Inputs'
import {Button} from '../atoms/Buttons'

import './CreateEventForm.css'

const CreateEventForm = ({onSubmit, onChange, ...props}) => {
  return (
    <div className="card">
      <form className="create_event_form" onSubmit={onSubmit} action="#">
        <Heading className="heading">Create new event</Heading>
        <SubHeading className="subheading">Enter details below.</SubHeading>
        <Input name="title" onChange={onChange} hint="Title" />
        <Input name="description" onChange={onChange} hint="Description" />
        <Input type="date" name="date" onChange={onChange} hint="Date" />
        <Input type="time" name="time" onChange={onChange} hint="Time" />
        <Input type="number" name="capacity" onChange={onChange} hint="Capacity" />
        <br /><br />
        <div className="button_container">
          <Button big text="CREATE NEW EVENT" />
        </div>
      </form>
    </div>
  );
};

export default CreateEventForm;


CreateEventForm.propTypes = {
  onSubmit : PropTypes.func,
  onChange : PropTypes.func
}