import React, { Component } from 'react';
import MediaQuery from 'react-responsive';
import { Layout, Header, Content } from '../hoc/Layout';
import { Link } from 'react-router-dom'
import { TextButton } from '../atoms/Buttons';
import CreateEventForm from '../templates/CreateEventForm'
import Api from '../../api'

import './CreatePage.css'

class CreatePage extends Component {
  constructor(props){
    super(props)
    
    this.state = {
      data : {
        title : '',
        description : '',
        date : '',
        time: '',
        capacity : 0
      },
      errors: {},
    }
    this.formSubmitted = this.formSubmitted.bind(this)
    this.formChanged = this.formChanged.bind(this)
  }

  formSubmitted(event){
    event.preventDefault();
    const eventObject = this.createEventObject(this.state.data)
    Api.post("/events", eventObject).then((response) => this.props.history.push('/'))
  }

  createEventObject(data){
    const startsAtString = new Date(data.date + " " + data.time).toISOString()
    return {
      title : data.title,
      description : data.description,
      startsAt : startsAtString,
      capacity : data.capacity
    }
  }

  formChanged(event){
    const field = event.target.name;
    const data = this.state.data;
    data[field] = event.target.value;

    this.setState({data});
  }

  render() {
    return (
      <Layout>
        <Header>
          <Link className="close_link" to="/">
            <MediaQuery minWidth={865}>
              <TextButton className="close_button" icon="close" text="Close"/>
            </MediaQuery>

            <MediaQuery maxWidth={864}>
              <TextButton className="close_button" icon="close"/>
            </MediaQuery>
          </Link>
        </Header>
        <Content>
          <CreateEventForm onSubmit={this.formSubmitted} onChange={this.formChanged}/>
        </Content>
      </Layout>
    );
  }
}

export default CreatePage;