import React, { Component } from 'react';
import MediaQuery from 'react-responsive'
import {Layout,Header,Content} from '../hoc/Layout';
import './Home.css'
import EventCardsContainer from '../templates/EventCardsContainer'
import {AddButton} from '../atoms/Buttons'
import {Link} from 'react-router-dom'
import UserDropDown from '../organisms/UserDropDown'

class Home extends Component {

  render() {
    return (
      <Layout>
        <Header>
          <MediaQuery minWidth={865}>
            <UserDropDown showText={true}/>
          </MediaQuery>
          <MediaQuery maxWidth={864}>
            <UserDropDown showText={false}/>
          </MediaQuery>
        </Header>
        <Content>
          <EventCardsContainer />
          <Link to="/create" className="create_event_link">
            <AddButton />
          </Link>
        </Content>
      </Layout>
    );
  }
}

export default Home;