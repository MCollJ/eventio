import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Layout, Header} from '../hoc/Layout'
import SideBannerImg from '../../resources/images/auth-banner@3x.jpg'
import {TextButton, Button} from '../atoms/Buttons'
import { Heading, Text } from '../atoms/Text';
import {Link} from 'react-router-dom'

import MediaQuery from 'react-responsive'

class ErrorPage extends Component {
  componentDidMount(){
    document.body.classList.add('trooper_background')
  }

  componentWillUnmount(){
    document.body.classList.remove('trooper_background')
  }
  render() {
    return (
      <Layout white>
        <Header>
            <MediaQuery minWidth={865}>
              <Text className="inline light_grey_font">Don't have an account? </Text>
              <TextButton className="bold" grey text="SIGN UP"/>
            </MediaQuery>
        </Header>
        <div className="left_banner">
          <img className="left_banner_img" src={SideBannerImg} alt="Side Banner" />
        </div>
        <div className="split_layout_content">
          <div className="split_layout_content_wrapper">
            <Heading>{this.props.heading}</Heading>
            <Text className="error_text">{this.props.children}</Text>
            <Link to={this.props.link ? this.props.link : '/'}><Button className="error_button" darkgrey big text="REFRESH"></Button></Link>
          </div>
        </div>
      </Layout>
    );
  }
};

export default ErrorPage;

ErrorPage.propTypes = {
  heading : PropTypes.string,
  link : PropTypes.string
}