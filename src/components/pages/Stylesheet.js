import React from 'react';

import {Button, TextButton} from '../atoms/Buttons'
import DropDown from '../molecules/DropDown'

import {Input} from '../atoms/Inputs'


const Stylesheet = () => {
  return (
    <div>
      <Button big text="SIGN IN"/>
     <br/>
      <Button text="JOIN"/>
     <br/>
      <Button pink text="LEAVE" />
     <br/>
      <Button grey text="EDIT" />
     <br/>
      <Button circle text="+" />
     <br/>
      <TextButton pink icon="delete" link="" text="DELETE EVENT" />
      <br/><br/>
      <Input hint="Email"/>
      <br/><br/>
      <DropDown text="dropdown"/>
    </div>
  );
};

export default Stylesheet;