import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

import SideBannerImg from '../../resources/images/auth-banner@3x.jpg'
import { TextButton } from '../atoms/Buttons'
import LoginForm from '../templates/LoginForm'
import { Redirect } from 'react-router-dom'

import { Layout, Header } from '../hoc/Layout';
import { Text } from '../atoms/Text';

class LoginPage extends Component {
  constructor(props){
    super(props)
    
    this.state = {
      redirectToSource: false
    }
  }

  render() {
    //Incase of redirect 
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    if(this.state.redirectToSource){
      return <Redirect to={from}/>
    }

    return (
      <Layout white>
        <Header>
          <MediaQuery minWidth={865}>
            <Text className="inline light_grey_font">Don't have an account? </Text>
            <TextButton className="bold" grey text="SIGN UP"/>
          </MediaQuery>
        </Header>
        <div className="left_banner">
          <img className="left_banner_img" src={SideBannerImg} alt="Side Banner" />
        </div>
        <div className="split_layout_content">
          <div className="split_layout_content_wrapper">
            <LoginForm setRedirect={() => this.setState({redirectToSource : true})}/>
          </div>
        </div>
      </Layout>
    );
  }
}

export default LoginPage;