import React from 'react';
import PropTypes from 'prop-types';
import './PopupMenu.css'
import {Text} from '../atoms/Text'

export const PopupMenu = (props) => {
  return (
    <div className="popup_menu">
      {props.children}
    </div>
  );
};

export const MenuItem = ({text, onClick, ...props}) => <button className={props.className} onClick={onClick}><Text>{text}</Text></button>

MenuItem.propTypes = {
  text : PropTypes.string,
  onClick : PropTypes.func
}