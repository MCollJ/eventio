import React from 'react';
import PropTypes from 'prop-types';

import './ButtonSwitcher.css'
import { TextButton } from '../atoms/Buttons';

const ButtonSwitcher = ({...props}) => {
  return (
    <div className={`button_switcher_wrapper ${props.className}`}>
      {props.children}
    </div>
  );
}

export const SwitchButton = ({text, onClick, ...props}) => {
  return (
    <TextButton onClick={onClick} className={`switcher_button ${props.className}`} text={text} />
  );
};

export default ButtonSwitcher;

SwitchButton.propTypes = {
  text : PropTypes.string,
  onClick : PropTypes.func
}