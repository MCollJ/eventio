import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {PopupMenu} from './PopupMenu'

class DropDown extends Component {

  constructor(props){
    super(props);
    this.state = {
      showMenu : false
    }
    this.showMenu = this.showMenu.bind(this);
    this.closeMenu = this.closeMenu.bind(this);
  }

  showMenu(e){
    e.preventDefault()
    this.setState({ showMenu: true }, () => document.addEventListener('click', this.closeMenu))
  }

  closeMenu(){
    this.setState({ showMenu: false }, () => document.removeEventListener('click', this.closeMenu))
  }

  componentWillUnmount(){
    document.removeEventListener('click', this.closeMenu)
  }

  render() {
    return (
      <span>
        <button onClick={this.showMenu} className={`button_dropdown grey_font ${this.props.className}`}>
        {this.props.showText ? this.props.text : null}
          <i className="material-icons">arrow_drop_down</i>
        </button>
        {this.state.showMenu
        ? (
        <PopupMenu>
          {this.props.children}
        </PopupMenu>
        ) : null}
      </span>
    );
  }
}

export default DropDown

DropDown.propTypes = {
  showText : PropTypes.bool,
  text: PropTypes.string
}