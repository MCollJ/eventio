import React from 'react';
import PropTypes from 'prop-types';
import Logo from "../../resources/icons/logo.png";
import {Link} from 'react-router-dom'

import './Layout.css'

export const Layout = ({white, ...props}) => {

    if (white) { document.body.classList.add('white_background') }
    else { document.body.classList.remove('white_background') }

    return (
        <div className="main_wrapper">
            {props.children}
        </div>
    );
};

export const Header = (props) => {
    return (
        <div className="header_wrapper">
            <Link to="/">
                <img src={Logo} alt="Logo"/>
            </Link>
            <div className="float_right">
                {props.children}
            </div>
        </div>
    );
};

export const Content = (props) => {
    return (
        <div className="content_wrapper">
            {props.children}
        </div>
    );
};

Layout.propTypes = {
    white : PropTypes.bool
}
