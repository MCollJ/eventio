import React, { Component } from 'react';
import PropTypes from 'prop-types';
import "./Inputs.css";

export class Input extends Component {

  render() {
    return (
      <div className="e-float-input" >
        <input name={this.props.name} onChange={this.props.onChange} type={this.props.type ? this.props.type : "text"} required={true} />
        <span className="e-float-line"/>
        <label className="e-float-text">{this.props.hint}</label>
      </div>
    );
  }

  floatFocus(args) {
    args.target.parentElement.classList.add('e-input-focus')
  }

  floatBlur(args) {
    args.target.parentElement.classList.remove('e-input-focus');
  }

}

Input.propTypes = {
  name : PropTypes.string,
  type : PropTypes.string,
  onChange : PropTypes.func,
  hint : PropTypes.string
}

