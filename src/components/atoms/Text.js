import React from 'react';
import PropTypes from 'prop-types';

import './Text.css'

export const Heading = (props) => {
  return (
    <h2 className={`heading_component ${props.className}`}>{props.children}</h2>
  );
};

export const SubHeading = (props) => {
  return (
    <h4 className={`sub_heading_component ${props.className}`}>{props.children}</h4>
  );
};

export const Text = (props) => <div className={`text_component ${props.className}`}>{props.children}</div>

export const IconText = ({icon, ...props}) => <span className={`icon_text ${props.className}`}><i className="material-icons">{icon}</i>{props.children}</span>

IconText.propTypes = {
  icon : PropTypes.string
}