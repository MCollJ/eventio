import React from 'react';
import PropTypes from 'prop-types';
import "./Buttons.css";

export const Button = ({onClick, big, pink, grey, darkgrey, circle, text, ...props}) => (
  <button {...props} className={`button
          ${big ? "button_big" : ''}
          ${pink ? "button_pink" : ''}
          ${grey ? "button_grey" : ''}
          ${circle ? "button_circle" : ''}
          ${darkgrey ? "button_darkgrey" : ''}
          ${props.className}`}
          onClick={onClick}>
    {text}
  </button>
);

export const AddButton = (props) => (
  <Button circle text="+" {...props}/>
);

export const TextButton = ({ onClick, icon, text, pink, grey, lightgrey, ...props }) => (
  <button {...props} onClick={onClick} className={`button_text 
          ${pink ? "pink_font" : ''}
          ${grey ? "grey_font" : ''}
          ${lightgrey ? "light_grey_font" : ''}
          ${props.className}`}>
    {icon ? <i className="material-icons">{icon}</i> : ''}
    {text}
  </button>
);

export const ImageButton = ({image, onClick, ...props}) => {
  return (
    <button className={props.className}>
      <img src={image} alt={props.alt} onClick={onClick} />
    </button>
  );
}


Button.propTypes = {
  link : PropTypes.string,
  big : PropTypes.bool,
  pink : PropTypes.bool,
  grey : PropTypes.bool,
  darkgrey : PropTypes.bool,
  circle : PropTypes.bool,
  text : PropTypes.string
}

TextButton.propTypes = {
  icon : PropTypes.string,
  text : PropTypes.string,
  pink : PropTypes.bool,
  grey : PropTypes.bool,
  lightgrey : PropTypes.bool,
}

ImageButton.propTypes = {
  image : PropTypes.string,
}