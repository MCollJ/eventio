class Auth {

  static setUser(user){
    localStorage.setItem('user', JSON.stringify(user))
  }

  static getUser(){
    return JSON.parse(localStorage.getItem('user'))
  }

  static setAuthToken(token){
    localStorage.setItem('token', token)
  }

  static setRefreshToken(token) {
    localStorage.setItem('refreshToken', token)
  }

  static hasToken(){
    return localStorage.getItem('token') != null;
  }

  static clearTokens(){
    localStorage.removeItem('token')
    localStorage.removeItem('refreshToken')
  }

  static getRefreshToken(){
    return localStorage.getItem("refreshToken")
  }

  static getAuthToken(){
    return  localStorage.getItem('token')
  }
}

export default Auth;